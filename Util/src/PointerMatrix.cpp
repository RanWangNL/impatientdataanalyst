//
// Created by user on 1-9-16.
//

#include "PointerMatrix.h"
#include <cstdlib>
#include <iostream>

PointerMatrixRowMajor::PointerMatrixRowMajor(size_t nRow_, size_t nCol_):nRow(nRow_), nCol(nCol_) {
    data = (double *)malloc(sizeof(double)*nRow*nCol);
}

PointerMatrixRowMajor::~PointerMatrixRowMajor() {
    free(data);
}


PointerMatrixColMajor::PointerMatrixColMajor(size_t nRow_, size_t nCol_):nRow(nRow_), nCol(nCol_) {
    data = (double *)malloc(sizeof(double)*nRow*nCol);
}



PointerMatrixColMajor::~PointerMatrixColMajor() {
    free(data);
}

PointerVector::PointerVector(size_t nRow_):nRow(nRow_) {
    data = (double *)malloc(sizeof(double)*nRow);
}

PointerVector::~PointerVector() {
    free(data);
}

void PointerMatrixRowMajor::print() {
    for (int i=0; i<nRow;i++){
        std::cout << this->operator()(i,0);
        for (int j=1;j<nCol;j++){
            std::cout << "," << this->operator()(i,j);
        }
        std::cout << std::endl;
    }
}

void PointerMatrixColMajor::print() {
    for (int i=0; i<nRow;i++){
        std::cout << this->operator()(i,0);
        for (int j=1;j<nCol;j++){
            std::cout << "," << this->operator()(i,j);
        }
        std::cout << std::endl;
    }
}

void PointerVector::print() {
    std::cout << this->operator()(0);
    for (int i =0; i<nRow;i++) {
        std::cout << this->operator()(i);
    }
    std::cout << std::endl;
}
