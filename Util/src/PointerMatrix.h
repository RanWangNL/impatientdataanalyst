//
// Created by user on 1-9-16.
//

#ifndef IMPATIENTDATAANALYST_POINTERMATRIX_H
#define IMPATIENTDATAANALYST_POINTERMATRIX_H
#include <cstdlib>


class PointerMatrixRowMajor {
public:
    double * data;
    size_t nRow;
    size_t nCol;
    PointerMatrixRowMajor(size_t nRow_, size_t nCol_);
    ~PointerMatrixRowMajor();
    inline double& operator()(size_t i, size_t j){
        return data[i*nCol+j];
    }
    void print();
};


class PointerMatrixColMajor{
public:
    double * data;
    size_t nRow;
    size_t nCol;
    PointerMatrixColMajor(size_t nRow_, size_t nCol_);
    ~PointerMatrixColMajor();
    inline double& operator()(size_t i, size_t j){
        return data[i+j*nRow];
    }
    void print();
};

class PointerVector{
public :
    double * data;
    size_t nRow;
    PointerVector(size_t nRow_);
    ~PointerVector();
    inline double& operator()(size_t i){
        return data[i];
    }
    void print();
};
#endif //IMPATIENTDATAANALYST_POINTERMATRIX_H
