//
// Created by user on 4-9-16.
//

#include "MatrixDecompositionDemonstration.h"
#include <Eigen/Dense>
#include <iostream>

using namespace Eigen;
void demonstrate_cholesky_decomposition() {
    std::cout << "Demonstrate Cholesky decomposition of of square positive-definite matrix " << std::endl;
    MatrixXd X = MatrixXd::Random(3,2);
    MatrixXd T = X.transpose() * X;
    std::cout << "The target matrix is" << std::endl;
    std::cout << T << std::endl;

    std::cout << "The Cholesky decomposition decompose a square positive-definite matrix T into L, such that L^TL = T " << std::endl;
    MatrixXd L = T.llt().matrixL();
    std::cout << "The decomposed matrix is" << std::endl;
    std::cout << L << std::endl;

    std::cout << "Confirm T = L^TL" <<std::endl;
    std::cout << L.transpose()*L  <<std::endl;
}




void demonstrate_svd() {
    std::cout << "Demonstrate SVD decomposition of a general matrix" << std::endl;
    MatrixXd T = MatrixXd::Random(3,2);
    std::cout << "The target matrix is" << std::endl << T <<std::endl;
    JacobiSVD<MatrixXd> svd(T, ComputeThinU | ComputeThinV);

    std::cout << "Singular values are " << std::endl << svd.singularValues() << std::endl;
    std::cout << "Left singular vectors are " << std::endl << svd.matrixU() <<std::endl;
    std::cout << "Right singular vector are " << std::endl << svd.matrixV() << std::endl;

    std::cout << "Verifying identities" << std::endl;
    std::cout << svd.matrixU()*svd.singularValues().asDiagonal()*svd.matrixV().transpose() << std::endl;
}

void demonstrate_qr() {
    std::cout << "Demonstrate QR decomposition of a general matrix" << std::endl;
    MatrixXd T = MatrixXd::Random(3,2);

    std::cout << "The target matrix is" << std::endl << T <<std::endl;
    ColPivHouseholderQR<MatrixXd> QR(T);
    MatrixXd P = QR.colsPermutation();
    MatrixXd Q = QR.matrixQ();
    MatrixXd R = QR.matrixR().triangularView<Upper>();
    std::cout << "The permutation matrix is " << std::endl << P <<std::endl;
    std::cout << "The Q matrix is " << std::endl << Q<< std::endl;
    std::cout << "The R matrix is " << std::endl << R << std::endl;

    std::cout << "Verifying identities" << std::endl;
    std::cout << "T*P = " << std::endl << T*P << std::endl;
    std::cout << "Q*R = " << std::endl << Q*R << std::endl;
}
