#include <iostream>
#include <Eigen/Dense>
#include <gsl/gsl_vector.h>
#include <cstdlib>
#include "../../Util/util.h"
#include "MatrixDecompositionDemonstration.h"

using namespace Eigen;
int main(){
    demonstrate_cholesky_decomposition();

    demonstrate_svd();

    demonstrate_qr();

    std::cout << "The rest demonstrate how to solve least squares problems using the above decompositions" << std::endl;
    MatrixXd X  = MatrixXd::Random(1000, 2);
    VectorXd y = VectorXd::Random(1000);

    std::cout << "Solving using normal equations" << std::endl;
    std::cout << (X.transpose()*X).llt().solve(X.transpose()*y) <<std::endl;


    std::cout <<"Solving using singular value decomposition" << std::endl;
    std::cout << X.jacobiSvd(ComputeThinU | ComputeThinV).solve(y) << std::endl;

    std::cout <<"Solving using QR decomposition " << std::endl;
    std::cout << X.colPivHouseholderQr().solve(y) <<std::endl;


    return 0;

}



